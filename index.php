<?php

use Google\Service\Drive\DriveFile;
use Google\Service\Drive;

require __DIR__ . '/vendor/autoload.php';

try {
    $client = new Google_Client();
    $client->setApplicationName('googletest');
    $client->addScope([Drive::DRIVE]);
    $client->useApplicationDefaultCredentials();
    $client->setAuthConfig(__DIR__ . '/credentials.json');
    $service = new Drive($client);

//    print_r( $service->files->get('1GSvEAEcOioI4GCaFr63NIswMcsiFaT2h'));die();
    $folderId = '1oeVojeqjuZ06S8n6M0FGSocJb1cPrrzY';
    $fileMetadata = new DriveFile(array('name' => 'test.txt', 'parents' => array($folderId)));

    $content = file_get_contents('test.txt');

    $file = $service->files->create($fileMetadata, array(
        'data' => $content,
        'mimeType' => 'text/plain',
        'fields' => 'id'));
    print($file->id);die();



//    $fileId = "1xk11S0NXOJTg1KeOK5zx44h6yWjjs-PjpKDcVzjtlQ0"; // Google File ID
//    $content = $service->files->get($fileId, array("alt" => "media"));
//    print_r($content);die();

} catch (Exception $e) {
    print_r($e->getMessage());
}
