<?php
ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);

use Google\Service\Sheets;
use Google\Service\Sheets\ClearValuesRequest;
use Google\Service\Sheets\ValueRange;
use Google\Service\Drive\DriveFile;
use Google\Service\Drive;

require __DIR__ . '/vendor/autoload.php';

try {
    $values[] = [1,1,1,1,1];
    $values[] = [1,5,3,3,5];
    $values[] = [3,3,4,3,1];
    $values[] = [2,2,2,2,2];
    $spreadsheetId = '1dEaSbbQxYuZrQUkmVoX9fPXHYvxGxekHnFl-3sFWWLc';
    $folderId = '1oeVojeqjuZ06S8n6M0FGSocJb1cPrrzY';

    $sheet = new GoogleSheetApi();
//    $sheet->setSpreadsheetId($spreadsheetId);
    $sheet->setSpreadsheetId($sheet->newSpreadsheet('new Sheet', $folderId));
    $sheet->setValues($values);

    //получить данные
//    print_r($sheet->get());

    //обновить данные
    $sheet->update();

    //вставить в конец таблицы
//    $sheet->insert();

    //удалить записи
//    $sheet->clear();

    //создать новую таблицу
//    print_r($sheet->newSpreadsheet());

    print_r($sheet->getSpreadsheetUrl());


} catch (Exception $e) {
    print_r($e->getMessage());
}


class GoogleSheetApi
{
    private $spreadsheetId;
    private $values;
    private $range = 'A1:Z1000';

    private $client;
    private $service;

    public function __construct()
    {
        $this->client = new \Google_Client();

        $this->client->setApplicationName('googletest');
        $this->client->setScopes([Sheets::SPREADSHEETS, Drive::DRIVE]);
        $this->client->setAccessType('offline');
        $this->client->setAuthConfig(__DIR__ . '/credentials.json');

        $this->service = new Sheets($this->client);
    }

    public function setSpreadsheetId(string $spreadsheetId): void
    {
        $this->spreadsheetId = $spreadsheetId;
    }

    public function setValues(array $values): void
    {
        $this->values = $values;
    }

    public function setRange(string $range): void
    {
        $this->range = $range;
    }

    public function getSpreadsheetId(): string
    {
        return $this->spreadsheetId;
    }

    public function getValues(): array
    {
        return $this->values;
    }

    public function getRange(): string
    {
        return $this->range;
    }

    public function getSpreadsheetUrl(): string
    {
        return 'https://docs.google.com/spreadsheets/d/' . $this->spreadsheetId . '/';
    }

    public function update(): bool
    {
        $this->service->spreadsheets_values->update(
            $this->spreadsheetId,
            $this->range,
            new ValueRange([
                'values' =>  $this->values
            ]),
            ['valueInputOption' => 'RAW']
        );

        return true;
    }

    public function insert(): bool
    {
        $this->service->spreadsheets_values->append(
            $this->spreadsheetId,
            $this->range,
            new ValueRange([
                'values' =>  $this->values
            ]),
            ['valueInputOption' => 'RAW']
        );

        return true;
    }

    public function get(): array
    {
        return $this->service->spreadsheets_values->get($this->spreadsheetId, $this->range)->values;
    }

    public function clear(): bool
    {
        $this->service->spreadsheets_values->clear($this->spreadsheetId, $this->range, new ClearValuesRequest());

        return true;
    }

    public function newSpreadsheet(string $sheetTitle, string $folderId): string
    {
        $driveService = new Drive($this->client);

        $fileMetadata = new DriveFile(array(
            'name' => $sheetTitle,
            'mimeType' => 'application/vnd.google-apps.spreadsheet', // указываем Mime-type файла
            'parents' => array($folderId)
        ));

        $file = $driveService->files->create($fileMetadata, array('fields' => 'id'));

        return $file->id;
    }
}


